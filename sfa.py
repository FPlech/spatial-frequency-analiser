from GUI import Ui_app
from PyQt5 import QtWidgets
import sys

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(ApplicationWindow, self).__init__()

        self.ui = Ui_app()
        self.ui.setupUi(self)

def main():
    app = QtWidgets.QApplication(sys.argv)
    application = ApplicationWindow()
    application.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()



    # def anyCheck_Box_checked(self):
    #     print("horizontal")

    # def browseButton_clicked(self):
    #     print("teste")

    # def setThresholdingLevel(self):
    #     print("slide")