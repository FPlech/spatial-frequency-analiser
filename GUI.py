# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GUI.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets, QtGui, Qt
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import cv2
import numpy as np
class Ui_app(object):

#**************************************************************************************
#                           USER INTERFACE CONFIGURATION
#**************************************************************************************
    def setupUi(self, app):
        self.secondTime = False
        self._toggle = True
        app.setObjectName("app")
        app.resize(740, 620)
        app.setMinimumSize(QtCore.QSize(740, 620))
        app.setMaximumSize(QtCore.QSize(740, 620))
        self.cfgGroup_box = QtWidgets.QGroupBox(app)
        self.cfgGroup_box.setGeometry(QtCore.QRect(410, 20, 321, 291))
        self.cfgGroup_box.setObjectName("cfgGroup_box")
        self.widget = QtWidgets.QWidget(self.cfgGroup_box)
        self.widget.setGeometry(QtCore.QRect(6, 30, 311, 251))
        self.widget.setObjectName("widget")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.magLabel = QtWidgets.QLabel(self.widget)
        self.magLabel.setObjectName("magLabel")
        self.horizontalLayout.addWidget(self.magLabel)
        self.magLineEdit = QtWidgets.QLineEdit(self.widget)
        self.magLineEdit.setText("")
        self.magLineEdit.setObjectName("magLineEdit")
        self.horizontalLayout.addWidget(self.magLineEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.arrayOrientationLabel = QtWidgets.QLabel(self.widget)
        self.arrayOrientationLabel.setObjectName("arrayOrientationLabel")
        self.verticalLayout.addWidget(self.arrayOrientationLabel)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horCheck_box = QtWidgets.QCheckBox(self.widget)
        self.horCheck_box.setObjectName("horCheck_box")
        self.horCheck_box.setChecked(self._toggle)
        self.horizontalLayout_3.addWidget(self.horCheck_box)
        self.verCheck_box = QtWidgets.QCheckBox(self.widget)
        self.verCheck_box.setObjectName("verCheck_box")
        self.horizontalLayout_3.addWidget(self.verCheck_box)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.verticalLayout_4.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.setImageFile_Label = QtWidgets.QLabel(self.widget)
        self.setImageFile_Label.setObjectName("setImageFile_Label")
        self.verticalLayout_2.addWidget(self.setImageFile_Label)
        self.setImageName_LineEdit = QtWidgets.QLineEdit(self.widget)
        self.setImageName_LineEdit.setObjectName("setImageName_LineEdit")
        self.verticalLayout_2.addWidget(self.setImageName_LineEdit)
        self.verticalLayout_4.addLayout(self.verticalLayout_2)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.threshSlideBar_Label = QtWidgets.QLabel(self.widget)
        self.threshSlideBar_Label.setObjectName("threshSlideBar_Label")
        self.horizontalLayout_4.addWidget(self.threshSlideBar_Label)
        self.threshSlider = QtWidgets.QSlider(self.widget)
        self.threshSlider.setMaximum(255)
        self.threshSlider.setOrientation(QtCore.Qt.Horizontal)
        self.threshSlider.setObjectName("threshSlider")
        self.horizontalLayout_4.addWidget(self.threshSlider)
        self.slideBar_label = QtWidgets.QLabel(self.widget)
        self.slideBar_label.setObjectName("slideBar_label")
        self.horizontalLayout_4.addWidget(self.slideBar_label)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.spatialFreq_label = QtWidgets.QLabel(self.widget)
        self.spatialFreq_label.setObjectName("spatialFreq_label")
        self.horizontalLayout_2.addWidget(self.spatialFreq_label)
        self.startProcess_button = QtWidgets.QPushButton(self.widget)
        self.startProcess_button.setObjectName("startProcess_button")
        self.horizontalLayout_2.addWidget(self.startProcess_button)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.graphGroup_box = QtWidgets.QGroupBox(app)
        self.graphGroup_box.setGeometry(QtCore.QRect(410, 320, 321, 291))
        self.graphGroup_box.setObjectName("graphGroup_box")
        self.graphPlot_Widget = QtWidgets.QWidget(self.graphGroup_box)
        self.graphPlot_Widget.setGeometry(QtCore.QRect(10, 30, 301, 251))
        self.graphPlot_Widget.setObjectName("graphPlot_Widget")
        self.layoutWidget = QtWidgets.QWidget(app)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 20, 371, 591))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.imgGroup_box_2 = QtWidgets.QGroupBox(self.layoutWidget)
        self.imgGroup_box_2.setObjectName("imgGroup_box_2")
        self.imgThresh_label = QtWidgets.QLabel(self.imgGroup_box_2)
        self.imgThresh_label.setGeometry(QtCore.QRect(20, 30, 321, 251))
        self.imgThresh_label.setText("")
        self.imgThresh_label.setObjectName("imgThresh_label")
        self.gridLayout.addWidget(self.imgGroup_box_2, 1, 0, 1, 1)
        self.imgGroup_box = QtWidgets.QGroupBox(self.layoutWidget)
        self.imgGroup_box.setObjectName("imgGroup_box")
        self.imgOriginal_label = QtWidgets.QLabel(self.imgGroup_box)
        self.imgOriginal_label.setGeometry(QtCore.QRect(20, 30, 321, 251))
        self.imgOriginal_label.mousePressEvent = self.getPixel
        self.imgOriginal_label.setText("")
        self.imgOriginal_label.setObjectName("imgOriginal_label")
        self.gridLayout.addWidget(self.imgGroup_box, 0, 0, 1, 1)

        self.retranslateUi(app)
        self.threshSlider.valueChanged['int'].connect(self.setThresholdingLevel)
        self.horCheck_box.clicked.connect(self.anyCheck_Box_checked)
        self.verCheck_box.clicked.connect(self.anyCheck_Box_checked)
        self.startProcess_button.clicked.connect(self.startProcess)
        QtCore.QMetaObject.connectSlotsByName(app)

        self.xImgSize = 321
        self.yImgSize = 251
        

    def retranslateUi(self, app):
        _translate = QtCore.QCoreApplication.translate
        app.setWindowTitle(_translate("app", "Spatial Frequency Analyser"))
        self.cfgGroup_box.setTitle(_translate("app", "Configuration"))
        self.magLabel.setText(_translate("app", "Magnification:"))
        self.arrayOrientationLabel.setText(_translate("app", "Array orientation:"))
        self.horCheck_box.setText(_translate("app", "Horizontal"))
        self.verCheck_box.setText(_translate("app", "Vertical"))
        self.setImageFile_Label.setText(_translate("app", "Insert image file name:"))
        self.threshSlideBar_Label.setText(_translate("app", "Thresholding level:"))
        self.slideBar_label.setText(_translate("app", "0"))
        self.label.setText(_translate("app", "Spatial Frequency (lp/mm):"))
        self.spatialFreq_label.setText(_translate("app", "0"))
        self.startProcess_button.setText(_translate("app", "Go"))
        self.graphGroup_box.setTitle(_translate("app", "Graph"))
        self.imgGroup_box_2.setTitle(_translate("app", "Thresholded Image"))
        self.imgGroup_box.setTitle(_translate("app", "Original Image"))

#**************************************************************************************
#                           CHECK BOX HANDLER
#**************************************************************************************
    def anyCheck_Box_checked(self):
        self._toggle = not self._toggle
        self.horCheck_box.setChecked(self._toggle)
        self.verCheck_box.setChecked(not self._toggle)

        self.horCheck_box.repaint()
        self.verCheck_box.repaint()

        self.secondTime = False

#**************************************************************************************
#                           START BUTTON HANDLER
#**************************************************************************************

    def startProcess(self):
        self.imgOriginal = cv2.imread(self.setImageName_LineEdit.text())
        #cv2.imshow("image",self.imgOriginal)
        self.qimage = QImage(self.imgOriginal, self.imgOriginal.shape[1], self.imgOriginal.shape[0],                                                                                                                                                 
                     QImage.Format_RGB888)
        
        self.pixmap = QPixmap(self.qimage)                                                                                                                                                                               
        self.pixmap = self.pixmap.scaled(self.xImgSize,self.yImgSize, QtCore.Qt.KeepAspectRatio)                                                                                                                                                    
        self.imgOriginal_label.setPixmap(self.pixmap)

        print(self.setImageName_LineEdit.text())

#**************************************************************************************
#                           SLIDER BAR HANDLER
#**************************************************************************************

    def setThresholdingLevel(self):
        self.threshLimit = self.threshSlider.value()
        self.slideBar_label.setText(str(self.threshLimit))

#**************************************************************************************
#                           DRAW THE FIRST LINE IN THE ORIGNAL IMAGE LABEL (VERTICAL)
#
# Input: Relative position of the click
# Output: None
#**************************************************************************************

    def paintLine_vertical(self):

        self.imgOriginalPixmap = np.copy(self.imgOriginal)

        self.realx = int(self.imgOriginalPixmap.shape[1]*self.relx)
        #realy = round(self.imgOriginalPixmap.shape[0]*self.rely)

        cv2.line(self.imgOriginalPixmap,(self.realx,0),(self.realx,self.imgOriginalPixmap.shape[0]),(255,0,0),1)

        self.qimage = QImage(self.imgOriginalPixmap, self.imgOriginalPixmap.shape[1], self.imgOriginalPixmap.shape[0],                                                                                                                                                 
                    QImage.Format_RGB888)

        self.pixmap = QPixmap(self.qimage)                                                                                                                                                                               
        self.pixmap = self.pixmap.scaled(self.xImgSize,self.yImgSize, QtCore.Qt.KeepAspectRatio)                                                                                                                                                    
        self.imgOriginal_label.setPixmap(self.pixmap)
        print("vertical")

#**************************************************************************************
#                           DRAW THE SECOND LINE IN THE ORIGNAL IMAGE LABEL (VERTICAL)
#
# Input: Relative position of the click
# Output: None
#**************************************************************************************

    def paintLine_vertical2(self):

        #self.imgOriginalPixmap = np.copy(self.imgOriginal)

        self.realx2 = int(self.imgOriginalPixmap.shape[1]*self.relx)
        #self.realy = round(self.imgOriginalPixmap.shape[0]*self.rely)

        cv2.line(self.imgOriginalPixmap,(self.realx2,0),(self.realx2,self.imgOriginalPixmap.shape[0]),(0,255,0),1)

        self.qimage = QImage(self.imgOriginalPixmap, self.imgOriginalPixmap.shape[1], self.imgOriginalPixmap.shape[0],                                                                                                                                                 
                    QImage.Format_RGB888)

        self.pixmap = QPixmap(self.qimage)                                                                                                                                                                               
        self.pixmap = self.pixmap.scaled(self.xImgSize,self.yImgSize, QtCore.Qt.KeepAspectRatio)                                                                                                                                                    
        self.imgOriginal_label.setPixmap(self.pixmap)
        print("vertical")

#**************************************************************************************
#                DRAW THE FIRST LINE IN THE ORIGNAL IMAGE LABEL (HORIZONTAL)
#
# Input: Relative position of the click
# Output: None
#**************************************************************************************

    def paintLine_horizontal(self):

        self.imgOriginalPixmap = np.copy(self.imgOriginal)

        #realx = round(self.imgOriginalPixmap.shape[1]*self.relx)
        self.realy = int(self.imgOriginalPixmap.shape[0]*self.rely)

        cv2.line(self.imgOriginalPixmap,(0,self.realy),(self.imgOriginalPixmap.shape[1],self.realy),(255,0,0),1)

        self.qimage = QImage(self.imgOriginalPixmap, self.imgOriginalPixmap.shape[1], self.imgOriginalPixmap.shape[0],                                                                                                                                                 
                    QImage.Format_RGB888)

        self.pixmap = QPixmap(self.qimage)                                                                                                                                                                               
        self.pixmap = self.pixmap.scaled(self.xImgSize,self.yImgSize, QtCore.Qt.KeepAspectRatio)                                                                                                                                                    
        self.imgOriginal_label.setPixmap(self.pixmap)
        print("vertical")

#**************************************************************************************
#              DRAW THE SECOND LINE IN THE ORIGNAL IMAGE LABEL (HORIZONTAL)
#
# Input: Relative position of the click
# Output: None
#**************************************************************************************

    def paintLine_horizontal2(self):

        #self.imgOriginalPixmap = np.copy(self.imgOriginal)

        #self.realx = round(self.imgOriginalPixmap.shape[1]*self.relx)
        self.realy2 = int(self.imgOriginalPixmap.shape[0]*self.rely)

        cv2.line(self.imgOriginalPixmap,(0,self.realy2),(self.imgOriginalPixmap.shape[1],self.realy2),(0,255,0),1)

        self.qimage = QImage(self.imgOriginalPixmap, self.imgOriginalPixmap.shape[1], self.imgOriginalPixmap.shape[0],                                                                                                                                                 
                    QImage.Format_RGB888)

        self.pixmap = QPixmap(self.qimage)                                                                                                                                                                               
        self.pixmap = self.pixmap.scaled(self.xImgSize,self.yImgSize, QtCore.Qt.KeepAspectRatio)                                                                                                                                                    
        self.imgOriginal_label.setPixmap(self.pixmap)
        print("vertical")


#**************************************************************************************
#                           ORIGINAL IMAGE CLICK HANDLER
#**************************************************************************************

    def getPixel(self, event):

        self.relx = event.pos().x()/self.xImgSize
        self.rely = event.pos().y()/self.yImgSize

        if self.horCheck_box.isChecked():
            if self.secondTime:
                self.paintLine_horizontal2()
                self.secondTime = not self.secondTime
            else:
                self.paintLine_horizontal()
                self.secondTime = not self.secondTime

        if self.verCheck_box.isChecked():
            
            if self.secondTime:
                self.paintLine_vertical2()
                self.secondTime = not self.secondTime
            else:
                self.paintLine_vertical()
                self.secondTime = not self.secondTime

            print("vertical")

        



        


        